from fastapi import FastAPI, File, UploadFile
from fastapi.responses import JSONResponse
import numpy as np
import cv2
import yaml
from yolo_pose.pose import yolo_pose

app = FastAPI()

config_path = "./configs/yolov8_pose_onnx.yaml"
with open(config_path, 'r') as f:
    config = yaml.load(f, Loader=yaml.FullLoader)
yolov8_onnx = yolo_pose.yolov8_pose(config)

@app.post("/predict/")
async def predict(file: UploadFile = File(...)):
    contents = await file.read()
    nparr = np.fromstring(contents, np.uint8)
    img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

    preds = yolov8_onnx.inference([img])
    boxes, scores, keypoints = preds

    results = []
    for box, score, kp in zip(boxes, scores, keypoints):
        result = {
            "box": box.tolist(),
            "score": score.tolist(),
            "keypoints": kp.tolist()
        }
        results.append(result)

    return JSONResponse(results)

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
