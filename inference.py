import cv2
import yaml
import os
from pose import yolo_pose  # Giả sử 'yolo_pose' là module có chứa class 'yolov8_pose'
POSE_PAIRS = [(0, 1), (1, 2),(2,3),(3,4),(4,5),(5,6)]

if __name__ == "__main__":
    config_path = "/home/luca/PycharmProjects/yolo_pose_onnx/yolo_pose/configs/yolov8_pose_onnx.yaml"
    input_dir = "/home/luca/PycharmProjects/yolo_pose_onnx/yolo_pose/images"
    output_dir = "Save data"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    image_files = [f for f in os.listdir(input_dir) if f.endswith('.jpg') or f.endswith('.png')]

    with open(config_path, 'r') as f:
        config = yaml.load(f, Loader=yaml.FullLoader)

    yolov8_onnx = yolo_pose.yolov8_pose(config)

    for image_file in image_files:
        image_path = os.path.join(input_dir, image_file)
        output_path = os.path.join(output_dir, image_file)

        img = cv2.imread(image_path)

        preds = yolov8_onnx.inference([img])
        boxes, scores, keypoints = preds
        print(preds)

        for box, kp in zip(boxes, keypoints):
            x_min, y_min, x_max, y_max = box
            cv2.rectangle(img, (int(x_min), int(y_min)), (int(x_max), int(y_max)), (0, 255, 0), 2)
            for x, y in kp:
                cv2.circle(img, (int(x), int(y)), 5, (0, 0, 255), thickness=-1)

            for pair in POSE_PAIRS:
                partA, partB = pair
                cv2.line(img, (int(kp[partA][0]), int(kp[partA][1])), (int(kp[partB][0]), int(kp[partB][1])),
                         (255, 0, 0), 2)

        cv2.imwrite(output_path, img)
        print(f"Processed and saved: {output_path}")

