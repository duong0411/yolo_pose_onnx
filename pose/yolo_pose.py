import cv2
import numpy as np
import onnxruntime
from typing import Union, Tuple, List
from yolo_pose.pose.utils import letterbox,scale_coords

class yolov8_pose:
    def __init__(self, config: dict) -> None:
        self.config = config
        self.onnx = self.config["onnx"]
        self.batch_size = self.config["batch_size"]
        self.size = self.config["size"]
        self.device = self.config["device"]
        self.confidence_threshold = self.config["confidence_threshold"]
        self.iou_threshold = self.config["iou_threshold"]
        self.num_keypoints = self.config["num_keypoints"]

        if self.device == "cpu":
            providers = ["CPUExecutionProvider"]
        elif self.device == "gpu":
            providers = ["CUDAExecutionProvider"]
        else:
            raise ValueError(f"Unsupported device: {self.device}")

        self.sess = onnxruntime.InferenceSession(self.onnx, providers=providers)
        self.input_name = self.sess.get_inputs()[0].name
        self.output_name = [output.name for output in self.sess.get_outputs()]

    def pre_process(self, images: List[np.ndarray]) -> Tuple[np.ndarray, List]:
        imgs = []
        ratios_pads = []
        for img in images:
            processed_img, ratio, pad = letterbox(img, new_shape=self.size, color=(114, 114, 114))
            processed_img = processed_img.transpose(2, 0, 1)[::-1]
            processed_img = processed_img / 255.0
            imgs.append(processed_img)
            ratios_pads.append((ratio, pad))


        imgs = np.array(imgs).astype(np.float32)
        print(imgs.shape)
        return imgs, ratios_pads

    def pose_postprocess(self, data: np.ndarray, img_shapes: List[Tuple[int, int]], ratios_pads: List) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        outputs = np.transpose(data, (0, 2, 1))
        all_bboxes, all_scores, all_kpts = [], [], []

        for i, output in enumerate(outputs):
            bboxes, scores, kpts = np.split(output, [4, 5], axis=1)
            scores = scores.squeeze()
            kpts = kpts.squeeze()

            idx = scores > self.confidence_threshold
            if not idx.any():
                continue

            bboxes, scores, kpts = bboxes[idx], scores[idx], kpts[idx]
            xycenter, wh = np.split(bboxes, [2], axis=-1)
            cvbboxes = np.concatenate([xycenter - 0.5 * wh, wh], axis=-1)
            idx = cv2.dnn.NMSBoxes(cvbboxes.tolist(), scores.tolist(), self.confidence_threshold, self.iou_threshold)
            print(idx)

            if len(idx) > 0:
                idx = idx.flatten()
                cvbboxes, scores, kpts = cvbboxes[idx], scores[idx], kpts[idx]
                cvbboxes[:, 2:] += cvbboxes[:, :2]


                # Scale the coordinates of the bounding boxes
                cvbboxes = scale_coords(self.size, cvbboxes, img_shapes[i], ratio_pad=ratios_pads[i])

                kpts = kpts.reshape(-1, self.num_keypoints // 2, 2)
                kpts = kpts.reshape(-1, 2)
                kpts = scale_coords(self.size, kpts, img_shapes[i], ratio_pad=ratios_pads[i], kpt_label=True, step=2)
                print(kpts)
                kpts = kpts.reshape(-1, self.num_keypoints //2 , 2)

                all_bboxes.append(cvbboxes)
                all_scores.append(scores)
                all_kpts.append(kpts)


        return np.concatenate(all_bboxes), np.concatenate(all_scores), np.concatenate(all_kpts)

    def inference(self, images: Union[np.ndarray, List[np.ndarray]]) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        if not isinstance(images, list):
            images = [images]
        processed_images, ratios_pads = self.pre_process(images)
        img_shapes = [img.shape[:2] for img in images]  # original image shapes
        model_out = self.sess.run(self.output_name, {self.input_name: processed_images})[0]
        pred = self.pose_postprocess(model_out, img_shapes, ratios_pads)
        return pred
